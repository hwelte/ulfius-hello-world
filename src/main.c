
#include <assert.h>
#include <stdbool.h>
#include <ulfius.h>

#define PREFIX "/hello-world"

static struct _u_instance g_instance;

static int api_cb_test(const struct _u_request *req, struct _u_response *resp, void *user_data)
{
	ulfius_set_string_body_response(resp, 200, "<html><body><h1>Hello World!</h1></body></html>\n");

	return U_CALLBACK_COMPLETE;
}

static const struct _u_endpoint api_ep = { "GET", PREFIX, "/test", 0, &api_cb_test, NULL };

int main(int argc, char **argv)
{
	assert(ulfius_init_instance(&g_instance, 11111, NULL, NULL) == U_OK);
	assert(ulfius_add_endpoint(&g_instance, &api_ep) == U_OK);
	assert(ulfius_start_framework(&g_instance) == U_OK);

	while (true) {
	}
}
